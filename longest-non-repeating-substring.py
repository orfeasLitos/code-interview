# input: 1 string
# output: number
def longest_nonrep_substr_len(string):
    # TODO

def test():
    assert(longest_nonrep_substr_len("word") == 4)
    assert(longest_nonrep_substr_len("abcdef") == 6)
    assert(longest_nonrep_substr_len("aaba") == 2)
    assert(longest_nonrep_substr_len("ccc") == 1)
    assert(longest_nonrep_substr_len("otoh") == 3)
    assert(longest_nonrep_substr_len("engrave") == 6)
    assert(longest_nonrep_substr_len("succulent") == 6)
    assert(longest_nonrep_substr_len("addididia") == 3)
    print("Success!")

test()
