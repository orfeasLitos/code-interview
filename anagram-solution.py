def char_counts(string):
    res = {}
    for char in string:
        if char in res:
            res[char] += 1
        else:
            res[char] = 1
    return res

# input: 2 strings
# output: bool
def are_anagrams(left, right):
    return char_counts(left) == char_counts(right)

def test():
    assert(are_anagrams("word", "word"))
    assert(are_anagrams("abcdef", "fedcba"))
    assert(are_anagrams("aaba", "abaa"))
    assert(are_anagrams("ccc", "ccc"))
    assert(not are_anagrams("fox", "dummy"))
    assert(not are_anagrams("seal", "seals"))
    assert(not are_anagrams("write", "wrote"))
    assert(not are_anagrams("aaa", "aaaa"))
    print("Success!")

test()
