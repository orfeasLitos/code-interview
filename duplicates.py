# input: 1 string
# output: list/set of chars
def duplicates(string):
    # TODO

def test():
    assert(set(duplicates("word")) == set())
    assert(set(duplicates("abcdef")) == set())
    assert(set(duplicates("aaba")) == set(['a']))
    assert(set(duplicates("ccc")) == set(['c']))
    assert(set(duplicates("otoh")) == set(['o']))
    assert(set(duplicates("succulent")) == set(['u', 'c']))
    assert(set(duplicates("addididia")) == set(['a', 'd', 'i']))
    print("Success!")

test()
