# input: 2 strings
# output: bool
def are_anagrams(left, right):
    # TODO

def test():
    assert(are_anagrams("word", "word"))
    assert(are_anagrams("abcdef", "fedcba"))
    assert(are_anagrams("aaba", "abaa"))
    assert(are_anagrams("ccc", "ccc"))
    assert(not are_anagrams("fox", "dummy"))
    assert(not are_anagrams("seal", "seals"))
    assert(not are_anagrams("write", "wrote"))
    assert(not are_anagrams("aaa", "aaaa"))
    print("Success!")

test()
