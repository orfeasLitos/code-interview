def char_counts(string):
    res = {}
    for char in string:
        if char in res:
            res[char] += 1
        else:
            res[char] = 1
    return res

def duplicates(string):
    counts = char_counts(string)
    return [ch for ch, count in counts.items() if count > 1]

# input: 1 string
# output: number
def longest_nonrep_substr_len(string):
    best = {'start': 0, 'end': 0}
    length = len(string)
    for start in range(0, length):
        if length - start <= best['end'] - best['start']:
            break
        for end in range(length, start, -1):
            print(start, end)
            if duplicates(string[start:end]) == []:
                if end - start > best['end'] - best['start']:
                    best['start'] = start
                    best['end'] = end
                    break
    return best['end'] - best['start']

def test():
    assert(longest_nonrep_substr_len("word") == 4)
    assert(longest_nonrep_substr_len("abcdef") == 6)
    assert(longest_nonrep_substr_len("aaba") == 2)
    assert(longest_nonrep_substr_len("ccc") == 1)
    assert(longest_nonrep_substr_len("otoh") == 3)
    assert(longest_nonrep_substr_len("engrave") == 6)
    assert(longest_nonrep_substr_len("succulent") == 6)
    assert(longest_nonrep_substr_len("addididia") == 3)
    print("Success!")

test()
