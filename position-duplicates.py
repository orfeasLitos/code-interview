# input: 1 string
# output: (char, sorted list of indexes) dictionary
def position_duplicates(string):
    # TODO

def test():
    assert(position_duplicates("word") == {})
    assert(position_duplicates("abcdef") == {})
    assert(position_duplicates("aaba") == {'a': [0, 1, 3]})
    assert(position_duplicates("ccc") == {'c': [0, 1, 2]})
    assert(position_duplicates("otoh") == {'o': [0, 2]})
    assert(position_duplicates("succulent") == {'u': [1, 4], 'c': [2, 3]})
    assert(position_duplicates("addididia") == 
        {'a': [0, 8], 'd': [1, 2, 4, 6], 'i': [3, 5, 7]}
    )
    print("Success!")

test()
