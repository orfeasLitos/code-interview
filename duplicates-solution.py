def char_counts(string):
    res = {}
    for char in string:
        if char in res:
            res[char] += 1
        else:
            res[char] = 1
    return res

# input: 1 string
# output: list/set of chars
def duplicates(string):
    counts = char_counts(string)
    return [ch for ch, count in counts.items() if count > 1]

def test():
    assert(set(duplicates("word")) == set())
    assert(set(duplicates("abcdef")) == set())
    assert(set(duplicates("aaba")) == set(['a']))
    assert(set(duplicates("ccc")) == set(['c']))
    assert(set(duplicates("otoh")) == set(['o']))
    assert(set(duplicates("succulent")) == set(['u', 'c']))
    assert(set(duplicates("addididia")) == set(['a', 'd', 'i']))
    print("Success!")

test()
